const express = require('express');
const fs = require('fs');
const path = require('path');

const router = express.Router();

router.get('', (req,res)=>{
    const dirPath = path.join(__dirname,'./files');
    
   fs.readdir(dirPath, (err, files)=>{
        if (err) {
            return res.status(500).json({ message: 'Server error' });
        }
        if(files.length === 0){
            return res.status(400).json({messege: 'Folder is empty'});
        }
        return res.status(200).json({ messege: 'Success', files });
    });
});

module.exports = router;