const express = require('express');
const path = require('path');
const fs = require('fs');

const router = express.Router();
const validNameFile = '^.*\\.(log|txt|json|yaml|xml|js)$';

router.post('', (req, res)=>{
    const{filename, content } = req.body;

    if(!content){
        return res.status(400).json({messege:`Please specify 'content' parameter`});
    }
    if(!filename){
        return res.status(400).json({messege:`Please specify 'filename' parameter`});
    }

    if(!filename.match(validNameFile)){
        return res.status(400).json({messege:`Wrong file type`});
    }
    fs.writeFile(`./files/${filename}`, content, function(err) {
            if(err) {
            return console.log('messege:' , err );
        }
        console.log("The file was saved!");
        return res.status(200).json(`${filename} was saved in files folder`);
    });
   
});


module.exports = router;