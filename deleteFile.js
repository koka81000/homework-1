const express = require('express');
const fs = require('fs');
const router = express.Router();
const path = require('path');



router.delete('/api/file/:filename', (req, res) => {
    const filename = req.params["filename"];
    fs.unlink(path.join('./files/', filename), (err) => {
        if (err) {
            if (err.code === 'ENOENT') {
                return res.status(400).json({
                    message: `No file with ${filename} filename found`,
                });
            }
            return res.status(500).json({ message: 'Server error' });
        }
        return res.status(200).json({ message: `The ${filename} was deleted` });
    });
});



   
module.exports = router;