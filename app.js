const express = require('express');
const createFile = require('./createFile.js');
const getFiles = require('./getFiles');
const getFile = require('./getFile');
const fs = require('fs');
const deleteFile = require('./deleteFile');
const morgan = require('morgan');
const app = express();


fs.stat('./files', (err)=>{
    if(err){
        fs.mkdir('./files', ()=>{
            console.log('Folder created');
            
        });
    }
});
    

app.use( express.json());
app.use(morgan('short'));

app.use( '/api/files/', createFile );

app.use( '/api/files/', getFiles );

app.use( '', getFile );

app.use('', deleteFile);

app.listen(8080,()=>{
    console.log( 'Server work at localhost:8080!' );
});