const express = require('express');
const fs = require('fs');
const router = express.Router();
const path = require('path');



router.get('/api/files/:filename', (req, res) => {
    const filename = req.params["filename"];

    fs.readFile(path.join('./files/', filename), 'utf-8', (err, data) => {
        if (err) {
            if (err.code === 'ENOENT') {
                return res.status(400).json({
                    message: `No file with ${filename} filename found`,
                });
            }
            return res.status(500).json({ message: 'Server error' });
        }
        fs.stat(path.join('./files/', filename), (err, stats) => {
            if (err) {
                return res.status(500).json({ message: 'Server error' });
            }
            return res.status(200).json({
                message: 'Success',
                filename,
                content: data,
                extension: path.extname(filename).slice(1),
                uploadedDate: stats.birthtime,
            });
        });
    });
});


   
module.exports = router;